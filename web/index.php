<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

require "../vendor/autoload.php";

echo 'Hello !<br /><br />';

$pdo = new PDO('mysql:host=db', 'root', 'root');
$pdo->query('CREATE DATABASE test') ;

echo "Créationd de la database : OK<br />";
echo "<br />";

$client = new Predis\Client('redis://redis');
$client->set('foo', 'bar');
$value = $client->get('foo');

echo "Connexion/Lecture/Ecriture Redis : OK<br />";
echo "<br />";

echo ";)";
